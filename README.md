# HygieniC++
There are 1000 ways to shoot yourself and the users of your software in the foot with C++.
Yet, it continues to be used and will be around for decades still. Be responsible, enforce
strict code hygiene when you chose to use C++.

## Status
Just an idea. Undeveloped since 2012.

## Milestones
* 0.0.1: Can enforce a small, useful, set of constraints that noone else can/does.
Initial target=concurrency? Usecase=MyPaint?


## Opinions
"C makes it easy to shoot yourself in the foot; C++ makes it harder,
but when you do it blows your whole leg off." B. Stroustrup, creator of C++.
http://www.codinghorror.com/blog/2007/01/the-problem-with-c.html

What most people define as "coding style" is formatting bikeshedding. It matters
little where the whitespace and braces go. A good codestyle avoids common pitfalls,
makes errors unlikely to occur or at the very least makes them easy to spot.


## Existing styles & best practices

* http://www.llvm.org/docs/CodingStandards.html
* ftp://ftp.idsoftware.com/idstuff/doom3/source/CodeStyleConventions.doc
* http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
* http://gcc.gnu.org/codingconventions.html
* http://www.gotw.ca/publications/c++cs.htm
* http://geosoft.no/development/cpppractice.html
* http://exceptionsafecode.com

## Existing tools

* http://oclint.org
* https://code.google.com/p/headerfile-free-cyclomatic-complexity-analyzer/
* http://google-styleguide.googlecode.com/svn/trunk/cpplint/
* http://wiki.eclipse.org/CDT/designs/StaticAnalysis
* http://splint.org
* http://cppcheck.sourceforge.net
* https://sparse.wiki.kernel.org/index.php/Main_Page
* https://github.com/dsw/oink-stack/
* http://sourceforge.net/projects/cccc/
* https://bitbucket.org/verateam/vera/wiki/Home

## Braindump

Concerns:

* Avoiding common programmer errors.
* Maintaining API and ABI compatibility.
* Maintaining loose coupling between different pieces of code.
* Avoiding code duplication.
* Keeping compilation time down.
* Keeping size of resulting binary down.
* Avoiding unnecessary and accidental performance degradation.

Tool guidelines:

* Sane defaults
* Large set of constraints available by default
* Minimal amount of false positives
* Designed for integration in IDEs, and for use in CI systems
* Easy to "guide" the tool in cases of ambiguity:
	through code annotations, using less esoteric programming constructs etc
* Maximize the amount of things that can be statically checked,
	but don't shy from dynamic approaches when useful
* Allow tracking of warnings, disabled warnings

Aspects

* Memory management
* Code structure and architecture
* Dependency management
* Real-time
* Concurrency
* Naming conventions

## Constraints Usecases

All headers have include guards
All C headers have C++ extern C guards

All files shall be compiled with -Wall -Werror

All heap allocated data structures must be managed by a smart pointer.

All member variables must be named "mSomething".
 alt: All member variables must be referred to using this->Something

All method which may be const should be const.
All variables which may be const should be const.

A class which is considered part of a public interface must use the pimpl pattern.
  alt: -- " --
  or have padding

A class which is considered part of a public interface must use getters and setters for all

A class which is supposed to be immutable may only have const methods

A class invariant is maintained between all public method calls

A class with pointer or reference members must either be non-copyable or have a custom copy constructor

No return by reference or pointer in method (makes changing it hard later)

Any symbol or definition which is considered part of a public interface must be prefixed with a prefix for the interface,
or be part of a namespace.
	classes, functions, enums, 

A private method which only accesses few member variables should be converted to a function in the anonymous
	or dedicated namespace, and be passed the members explicitly (where 'few' may be <=3)
	! does this conflict with unit testing?

Functions which are symmetric to eachother shall be consecutive in the code, constructive action first
The length of identifiers shall be proportional to their scope

Pure virtual methods are only allowed in abstract classes.
Introduction of virtual methods are only allowed in abstract classes.

Multiple inheritance is only allowed with 1 non-abstract base class.

Cyclomatic complexity must be lower than N (where 'N' may be 5)
All for/if/else must have {} for the enclosing block
No side-effects allowed in statements
Absolutely no side effects in statements that depend on branches

No include of files when a forward-declaration would do

No exceptions in certain part of code
No throwing of undeclared exceptions (note: standard exception declarators are broken this way)
No throwing of non-specialised exceptions
No generic exception handler

-- requires that functions and code paths can be annotated --
Unless otherwise marked as safe, functions may not be called from multiple threads.
No non-reentrant functions may not be used in multithreaded code.
No functions with unpredictable execution time may be used in real-time code paths. Examples: malloc, free

? Whitelisting versus blacklisting
? Warnings on areas with missing annotations/info

User-provided pre and post-conditions with automatic verification
Automatically applying pre and post-conditions to overridden methods

No exit outside of main

## Quality metrics
Ratio of public interface to implementation (less is better)

## Implementation
Clang plugin?

* http://clang.llvm.org/docs/Tooling.html
* http://code.google.com/p/chromium/wiki/WritingClangPlugins
* http://clang.llvm.org/docs/ClangPlugins.html
* http://clang-analyzer.llvm.org/checker_dev_manual.html

## Naming fun

ConcurrencyContraception: preventing concurrency bugs from being born.


## Research

### Compilation-time-per-SLOC.
How big are the differences between different projects?
For given tech choices, what would be a "good" build time?
If it is bad, which projects and practices to look at for fixing it?
Is there correlation with:
build system? make,automake,cmake
programming language? C,C++
specific build opt techniques? reducing header include, non-recursive make
Could use Arch Linux ABS/AUR as a base for builds.
ohcount could be used to gather SLOC
Would need to extract time spent in the different build phases
Caveats: feature flags, not everything has separate "build" and "install" phase

References: http://rusty.ozlabs.org/?p=330

## Related

* http://en.wikipedia.org/wiki/Design_by_contract
* Code Contracts for .NET: http://visualstudiogallery.msdn.microsoft.com/1ec7db13-3363-46c9-851f-1ce455f66970
* Contract Programming in D: http://ddili.org/ders/d.en/contracts.html
* C++ library: https://github.com/alexeiz/contract
* C/C++ support in Digital Mars compiler: http://www.digitalmars.com/ctg/contract.html
* http://www.codeproject.com/Articles/8293/Design-by-Contract-in-C
* Standard proposal: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1962.html
* http://www.eventhelix.com/realtimemantra/object_oriented/design_by_contract.htm#.U0KrwdxIyMk
* http://www.drdobbs.com/cpp/programming-with-contracts-in-c/184405997

Could one use a C++11 class which takes this + two functions (receiving this) for pre+post-con?
http://lucabolognese.wordpress.com/2012/03/06/a-simple-scheme-to-implement-design-by-contract-in-c/